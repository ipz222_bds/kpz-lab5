namespace HTMLOfDreams
{
    public interface IVisitor
    {
        void Visit(LightElementNode element);
        void Visit(LightTextNode textNode);
    }

    public class RenderVisitor : IVisitor
    {
        public void Visit(LightElementNode element)
        {
            Console.WriteLine($"Rendering element: {element.TagName}");
        }

        public void Visit(LightTextNode textNode)
        {
            Console.WriteLine($"Rendering text: {textNode.Text}");
        }
    }
}
