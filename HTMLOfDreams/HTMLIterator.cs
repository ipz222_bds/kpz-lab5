using System.Collections;
using System.Collections.Generic;

namespace HTMLOfDreams
{
    public class HTMLIterator : IEnumerable<LightNode>
    {
        private LightNode root;

        public HTMLIterator(LightNode root)
        {
            this.root = root;
        }

        public IEnumerator<LightNode> GetEnumerator()
        {
            return TraverseDepthFirst(root).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        private IEnumerable<LightNode> TraverseDepthFirst(LightNode node)
        {
            yield return node;
            if (node is LightElementNode element)
            {
                foreach (var child in element.Children)
                {
                    foreach (var descendant in TraverseDepthFirst(child))
                    {
                        yield return descendant;
                    }
                }
            }
        }

        private IEnumerable<LightNode> TraverseBreadthFirst(LightNode node)
        {
            var queue = new Queue<LightNode>();
            queue.Enqueue(node);
            while (queue.Count > 0)
            {
                var current = queue.Dequeue();
                yield return current;
                if (current is LightElementNode element)
                {
                    foreach (var child in element.Children)
                    {
                        queue.Enqueue(child);
                    }
                }
            }
        }
    }
}
