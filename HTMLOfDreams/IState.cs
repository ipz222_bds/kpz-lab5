namespace HTMLOfDreams
{
    public interface IState
    {
        void Apply(LightElementNode element);
    }

    public class VisibleState : IState
    {
        public void Apply(LightElementNode element)
        {
            element.CssClasses.Remove("hidden");
            element.CssClasses.Add("visible");
        }
    }

    public class HiddenState : IState
    {
        public void Apply(LightElementNode element)
        {
            element.CssClasses.Remove("visible");
            element.CssClasses.Add("hidden");
        }
    }
}
