﻿using System;

namespace HTMLOfDreams
{
    class Program
    {
        static void Main(string[] args)
        {
            // Створення HTML дерева
            LightElementNode div = new LightElementNode("div", true, false);
            div.CssClasses.Add("container");

            LightElementNode p = new LightElementNode("p", true, false);
            p.Children.Add(new LightTextNode("This is a paragraph."));
            
            LightElementNode img = new LightElementNode("img", false, true);
            img.CssClasses.Add("responsive");

            div.Children.Add(p);
            div.Children.Add(img);

            // Використання Ітератора
            HTMLIterator iterator = new HTMLIterator(div);
            foreach (var node in iterator)
            {
                Console.WriteLine(node.GenerateHTML());
            }

            // Використання Команди
            ICommand addClassCommand = new AddClassCommand(p, "new-class");
            addClassCommand.Execute();
            Console.WriteLine(div.GenerateHTML());

            // Використання Стейта
            IState hiddenState = new HiddenState();
            hiddenState.Apply(div);
            Console.WriteLine(div.GenerateHTML());

            // Використання Відвідувача
            IVisitor renderVisitor = new RenderVisitor();
            div.Accept(renderVisitor);
        }
    }
}
