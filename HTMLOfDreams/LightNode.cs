namespace HTMLOfDreams
{
    public abstract class LightNode
    {
        public abstract string OuterHTML { get; }
        public abstract string InnerHTML { get; }
        public abstract void Accept(IVisitor visitor);

        public virtual void OnCreated() { }
        public virtual void OnInserted() { }
        public virtual void OnRemoved() { }
        public virtual void OnStylesApplied() { }
        public virtual void OnClassListApplied() { }
        public virtual void OnTextRendered() { }

        public string GenerateHTML()
        {
            OnCreated();
            string outerHtml = OuterHTML;
            OnInserted();
            OnRemoved();
            return outerHtml;
        }
    }
}
